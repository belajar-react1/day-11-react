import React, { useEffect, useState } from 'react';
import Navbar from './assets/components/Navbar';
import Sidebar from './assets/components/Sidebar';
import { Box, List } from '@mui/material';
import SidebarList from './assets/components/SidebarList';
import InsertPhotoRoundedIcon from "@mui/icons-material/InsertPhotoRounded";
import ContentCard from './assets/components/ContentCard';
import axios from 'axios';
import { useFetch } from './custom_hooks/useFetch';

const App = () => {

  const [contents, setContents] = useState([]);

  // custom hooks
  const { data, error } = useFetch("http://localhost:3004/postgenerated");

  useEffect(() => {
    console.log(data);
    setContents(data);
  }, [data]);

  return (
    <>
      <Navbar />
      <Box sx={{ display: "flex", marginTop: 5, gap: 3, justifyContent: "center", paddingX: 2, paddingY: 3 }}>
        <Box sx={{ display: "grid", gridTemplateColumns: { sm: 'repeat(2, 1fr)', xs: 'repeat(1, 1fr)' }, gap: 2 }} >
          {contents?.map((c, i) => (
            <ContentCard key={i} {...c} />
          ))}
        </Box>
        <Sidebar>
          <List>
            <SidebarList
              icon={<InsertPhotoRoundedIcon />}
              primaryText={"Galery"}
              secondaryText={"Jan 8, 2023"}
            />
            <SidebarList
              icon={<InsertPhotoRoundedIcon />}
              primaryText={"Activity"}
              secondaryText={"Jan 8, 2023"}
            />
            <SidebarList
              icon={<InsertPhotoRoundedIcon />}
              primaryText={"Test"}
              secondaryText={"Jan 8, 2023"}
            />
          </List>
        </Sidebar>
      </Box >
    </>
  )
}

export default App