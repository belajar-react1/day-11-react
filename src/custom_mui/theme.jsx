import { createTheme } from "@mui/material";

const envColor = process.env.REACT_APP_COLOR_PRIMARY;

const theme = createTheme({
  palette: {
    primary: {
      main: `${envColor}`,
    },
    secondary: {
      main: `${envColor}`,
    },
  },
});

export default theme;
