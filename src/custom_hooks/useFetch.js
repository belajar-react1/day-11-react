import React, { useState, useEffect } from "react";

export const useFetch = (url) => {

    const [datas, setDatas] = useState({
        data: null,
        error: null,
    });

    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then(res => {
                setDatas({ ...datas, data: res });
            })
            .catch(err => {
                setDatas({ ...datas, error: err });
            });
    }, []);

    useEffect(() => {
        console.log(datas);
    }, [datas]);

    return {
        ...datas
    };
};