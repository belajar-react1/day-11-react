import {
  AppBar,
  Box,
  Button,
  Container,
  Link,
  Menu,
  MenuItem,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import AdbIcon from "@mui/icons-material/Adb";
import MenuIcon from "@mui/icons-material/Menu";

const Navbar = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar position="static" className="px-5 py-3" color="primary">
      <Stack
        direction={"row"}
        alignItems={"center"}
        justifyContent={"space-between"}
        className="w-full"
      >
        <Stack direction={"row"} paddingLeft={5} maxWidth="xl">
          <Toolbar disableGutters>
            <AdbIcon sx={{ mr: 1 }} />
            <Typography
              variant="h6"
              noWrap
              component={"a"}
              href="/"
              sx={{
                mr: 2,
                display: { xs: "none", md: "flex" },
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "inherit",
                textDecoration: "none",
              }}
            >
              MY APP
            </Typography>
            <Box
              sx={{
                display: { xs: "none", sm: "none", md: "flex" },
                gap: 3,
                marginLeft: 10,
              }}
            >
              <Typography
                variant="body1"
                component="a"
                href="/"
                sx={{
                  mr: 2,
                  fontWeight: 600,
                  color: "yellow",
                  fontSize: 20,
                }}
              >
                About
              </Typography>
              <Typography
                variant="body1"
                component="a"
                href="/"
                sx={{
                  mr: 2,
                  fontWeight: 600,
                  color: "yellow",
                  fontSize: 20,
                }}
              >
                Memory
              </Typography>
            </Box>
          </Toolbar>
        </Stack>
        <Stack
          sx={{ display: { md: "none", sm: "flex" } }}
          direction={"row"}
          justifyContent={"left"}
          marginRight={"0.1rem"}
        >
          <Button variant="text" sx={{ color: "white" }} onClick={handleClick}>
            <MenuIcon fontSize="large" />
          </Button>
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            <Link href="/">
              <MenuItem onClick={handleClose}>About</MenuItem>
            </Link>
            <Link href="/">
              <MenuItem onClick={handleClose}>Memory</MenuItem>
            </Link>
          </Menu>
        </Stack>
      </Stack>
    </AppBar>
  );
};

export default Navbar;
