import { Box } from "@mui/material";
import React from "react";
import PropTypes from "prop-types";

const Sidebar = ({ children }) => {
  return (
    <>
      <Box
        sx={{
          display: {
            xs: "none",
            sm: "none",
            md: "flex",
          },
          flexDirection: "column",
          width: 400,
          height: "100vh",
          boxShadow: 1,
          padding: 5,
        }}
      >
        {children}
      </Box>
    </>
  );
};

// Sidebar.propTypes = {
//   children: PropTypes.object,
// };

export default Sidebar;
