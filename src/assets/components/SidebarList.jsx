import {
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import React from "react";
import PropTypes from "prop-types";

const SidebarList = ({ icon, primaryText, secondaryText }) => {
  return (
    <ListItem disablePadding sx={{ marginBottom: 1 }}>
      <ListItemButton>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={primaryText} secondary={secondaryText} />
      </ListItemButton>
    </ListItem>
  );
};

// SidebarList.propTypes = {
//   icon: PropTypes.object,
//   primaryText: PropTypes.string,
//   secondaryText: PropTypes.string,
// };

export default SidebarList;
