import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  IconButton,
  Typography,
} from "@mui/material";
import React from "react";
import { red } from "@mui/material/colors";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import PropTypes from "prop-types";


const ContentCard = ({
  post_id,
  img,
  title,
  author,
  lastName,
  datePost,
  description,
}) => {
  return (
    <Card sx={{ maxWidth: 450 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            {author && author[0].toUpperCase()}
          </Avatar>
        }
        title={title}
        subheader={datePost}
      />
      <CardMedia component="img" height="150px" image={img} alt={title} />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
      </CardContent>
    </Card>
  );
};

ContentCard.propTypes = {
  post_id: PropTypes.string,
  img: PropTypes.string,
  title: PropTypes.string,
  author: PropTypes.string,
  lastName: PropTypes.string,
  datePost: PropTypes.string,
  description: PropTypes.string,
};

export default ContentCard;
